all : main/main.cpp lib/libb.so lib/liba.so
	export PATH=$PATH:$(shell pwd)
	ln -s ./liba.so ./lib/liba.dll
	ln -s ./libb.so ./lib/libb.dll
	gcc main/main.cpp -L./lib -la -lb -o main/main

s/s.o : s/s.cpp
	gcc -c -fpic s/s.cpp -o s/s.o

a/a.o : a/a.cpp
	gcc -c -fpic a/a.cpp -o a/a.o

b/b.o : b/b.cpp
	gcc -c -fpic b/b.cpp -o b/b.o

lib/liba.so : a/a.o s/s.o
	gcc -shared -o lib/liba.so a/a.o s/s.o

lib/libb.so : b/b.o s/s.o
	gcc -shared -o lib/libb.so b/b.o s/s.o

clean :
	rm a/a.o b/b.o s/s.o main/main lib/lib*
