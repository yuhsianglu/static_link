#include "../b/b.h"
#include "../a/a.h"

#include <stdio.h>

int main(int argc, char** argv) {
  printf("A: %p\n", AGetInstaceAddr());
  printf("B: %p\n", BGetInstaceAddr());

  return 0;

}
